<?php

class Uipl_SendOrderCancel_Model_Observer {

    public function sendcancelordermail(Varien_Event_Observer $observer) {

        $order = $observer->getEvent()->getOrder();
      	 $order->getId();
	
         $cusomerEmail = $order->getCustomerEmail();/* Receiver Email */
        
        $cusomerFirstName = $order->getCustomerFirstname();
        $cusomerLastName = $order->getCustomerLastname();
        $customerName = $cusomerFirstName . ' ' . $cusomerLastName ;/* Receiver Name */
       
	     $storename=Mage::getStoreConfig('trans_email/ident_sales/name');  /* Sender Name */
	   
	     $storeemail=Mage::getStoreConfig('trans_email/ident_sales/email'); /* Sender Email */
	   
	    $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('custom_email_after_order_cancel');
	  
	    $emailTemplate->setSenderEmail($storeemail);
	    $emailTemplate->setSenderName($storename);
	    
	    $templateParams=array("ordercancelconfirm"=>$order,"customername"=>$customerName,"email"=>$cusomerEmail);

	    $emailTemplate->send($cusomerEmail,$customerName,$templateParams);
	    
          return $this;   
                                
                               
       
    }



    public function sendorderconfirmmail($observer)
    {
    	$order = $observer->getEvent()->getOrder();
        $order->getId();
	
         $cusomerEmail = $order->getCustomerEmail();/* Receiver Email */
        
        $cusomerFirstName = $order->getCustomerFirstname();
        $cusomerLastName = $order->getCustomerLastname();
        $customerName = $cusomerFirstName . ' ' . $cusomerLastName ;/* Receiver Name */
       
	     $storename=Mage::getStoreConfig('trans_email/ident_sales/name');  /* Sender Name */
	   
	     $storeemail=Mage::getStoreConfig('trans_email/ident_sales/email'); /* Sender Email */
	     $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('after_payment_email');
	  
	    $emailTemplate->setSenderEmail($storeemail);
	    $emailTemplate->setSenderName($storename);

	    $templateParams=array("orderconfirm"=>$order,"customername"=>$customerName,"email"=>$cusomerEmail);
		$emailTemplate->send($cusomerEmail,$customerName,$templateParams);

        return $this;  // always return $this.
    }

}
