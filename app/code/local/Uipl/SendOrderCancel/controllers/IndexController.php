<?php
class Uipl_SendOrderCancel_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		   ));

      $this->renderLayout(); 
	  
    }


    public function SendmailAction() {  

      $post = $this->getRequest()->getPost();
      
      $orderId = $post['orderid'];
      $order = Mage::getModel('sales/order')->load($orderId);
      $cusomerEmail = $order->getCustomerEmail();/* Receiver Email */
      $cusomerFirstName = $order->getCustomerFirstname();
      $cusomerLastName = $order->getCustomerLastname();
      $customerName = $cusomerFirstName . ' ' . $cusomerLastName ;/* Receiver Name */
      $status = $order->getStatus();

      if($status == "pending"){
       
        $newStatus = $order->setStatus('cancel_notice');
        $order->save();
      }
      if($status == "processing"){
       
        $newStatus = $order->setStatus('cancel_notice');
        $order->save();
      }

      $emailTemplate = Mage::getModel('core/email_template')->loadDefault('cancel_order_email');
       
      $storename=Mage::getStoreConfig('trans_email/ident_sales/name');  /* Receiver Name */
      
      $storeemail=Mage::getStoreConfig('trans_email/ident_sales/email'); /* Receiver Email */
     
      $emailTemplate->setSenderName($customerName);  /* Sender Name */
      $emailTemplate->setSenderEmail($cusomerEmail); /* Sender Email */
      
      $templateParams=array("ordercancel"=>$order,"customername"=>$customerName,"email"=>$cusomerEmail,"comment"=>$post['comment']);
     
      $emailTemplate->send($storeemail, $storename, $templateParams);
      //Mage::getSingleton('core/session')->addSuccess('Order cancellation request sent to administartor successfully.');
      echo 'Order cancellation request sent to administartor successfully.';
      exit;
     //echo oktest;
     
  }
  


}