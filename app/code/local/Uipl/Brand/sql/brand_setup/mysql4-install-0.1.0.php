<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table brand(brand_id int not null auto_increment, 
title varchar(255) NOT NULL,
image varchar(255) NOT NULL,
details text NOT NULL,
url varchar(255) NOT NULL,
attribute_id int (11) NOT NULL,
status int (11) NOT NULL,
 primary key(brand_id));
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 