<?php
class Uipl_Brand_Block_Adminhtml_Brand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("brand_form", array("legend"=>Mage::helper("brand")->__("Item information")));

				
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("brand")->__("Title"),
						"name" => "title",
						));
									
						$fieldset->addField('image', 'image', array(
						'label' => Mage::helper('brand')->__('Image'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));
						$fieldset->addField("details", "textarea", array(
						"label" => Mage::helper("brand")->__("Content"),
						"name" => "details",
						));
					
						$fieldset->addField("url", "text", array(
						"label" => Mage::helper("brand")->__("URL"),
						"name" => "url",
						));
						
						 $fieldset->addField('attribute_id', 'select', array(
						'label'     => Mage::helper('brand')->__('Brand Attribute'),
						'values'   => Uipl_Brand_Block_Adminhtml_Brand_Grid::getValueArray5(),
						'name' => 'attribute_id',
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('brand')->__('Status'),
						'values'   => Uipl_Brand_Block_Adminhtml_Brand_Grid::getValueArray4(),
						'name' => 'status',
						));

				if (Mage::getSingleton("adminhtml/session")->getBrandData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getBrandData());
					Mage::getSingleton("adminhtml/session")->setBrandData(null);
				} 
				elseif(Mage::registry("brand_data")) {
				    $form->setValues(Mage::registry("brand_data")->getData());
				}
				return parent::_prepareForm();
		}
}
