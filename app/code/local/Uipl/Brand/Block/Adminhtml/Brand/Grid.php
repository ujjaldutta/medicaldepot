<?php

class Uipl_Brand_Block_Adminhtml_Brand_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("brandGrid");
				$this->setDefaultSort("brand_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("brand/brand")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("brand_id", array(
				"header" => Mage::helper("brand")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "brand_id",
				));
                
				$this->addColumn("title", array(
				"header" => Mage::helper("brand")->__("Title"),
				"index" => "title",
				));
				$this->addColumn("url", array(
				"header" => Mage::helper("brand")->__("URL"),
				"index" => "url",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('brand')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Uipl_Brand_Block_Adminhtml_Brand_Grid::getOptionArray4(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('brand_id');
			$this->getMassactionBlock()->setFormFieldName('brand_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_brand', array(
					 'label'=> Mage::helper('brand')->__('Remove Brand'),
					 'url'  => $this->getUrl('*/adminhtml_brand/massRemove'),
					 'confirm' => Mage::helper('brand')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='Enabled';
			$data_array[1]='Disabled';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Uipl_Brand_Block_Adminhtml_Brand_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getValueArray5(){
			$attribute_code = "storebrand";
			 $data_array=array();
			$attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attribute_code);
			$options = $attribute_details->getSource()->getAllOptions(false);
			foreach($options as $option){
				
				$data_array[]=array('value'=>$option["value"],'label'=> $option["label"]);
				}
				return($data_array);
				
		}
		

}