<?php

class Uipl_Brand_Model_Layer extends Mage_Catalog_Model_Layer
 	{
	 public function getProductCollection()
    {
        if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
            $collection = $this->getCurrentCategory()->getProductCollection();
            $this->prepareProductCollection($collection);
            $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
        }
        if(!empty($_GET["storebrand"])){
        $collection->addAttributeToFilter("storebrand",array('eq'=>$_GET["storebrand"]));
        }
        
        return $collection;
    }
 	}
?>