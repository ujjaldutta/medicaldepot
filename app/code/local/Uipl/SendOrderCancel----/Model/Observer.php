<?php

class Uipl_SendOrderCancel_Model_Observer {

    public function sendcancelordermail(Varien_Event_Observer $observer) {

        $order = $observer->getEvent()->getOrder();
      	 $order->getId();
	
         $cusomerEmail = $order->getCustomerEmail();/* Receiver Email */
        
        $cusomerFirstName = $order->getCustomerFirstname();
        $cusomerLastName = $order->getCustomerLastname();
        $customerName = $cusomerFirstName . ' ' . $cusomerLastName ;/* Receiver Name */
       
	     $storename=Mage::getStoreConfig('trans_email/ident_sales/name');  /* Sender Name */
	   
	     $storeemail=Mage::getStoreConfig('trans_email/ident_sales/email'); /* Sender Email */
	   
	    $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('custom_email_after_order_cancel');
	  
	    $emailTemplate->setSenderEmail($storeemail);
	    $emailTemplate->setSenderName($storename);
	    
	    $templateParams=array("ordercancelconfirm"=>$order,"customername"=>$customerName,"email"=>$cusomerEmail);

	    $emailTemplate->send($cusomerEmail,$customerName,$templateParams);
	    
           
                                
                               
       
    }

}
