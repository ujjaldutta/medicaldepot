<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Uipl_Banner_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Banner"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("banner", array(
                "label" => $this->__("Banner"),
                "title" => $this->__("Banner")
		   ));

      $this->renderLayout(); 
	  
    }
    
   function testAction(){
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
			/* ************* expire in 6 month ************ */
			$collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			
			
		
			$collection->addAttributeToFilter('expiry_date', array(
			    'from' => Date('Y-m-d', strtotime("+ 6 Months")),
			    'to' =>Date('Y-m-d', strtotime("+ 7 Months")),
			    'date' => true,
			    ));
			
			
			$expro='<table width="100%" cellpadding="10" cellspacing="10">';
			
			
			
			if($collection->count()>0){
			    $expro .='<tr><td colspan="4">Following product will expire after 6 months</td>
			    <tr><td>Product Name</td><td>SKU</td><td>Exipire Date</td></tr>
			    ';
			    foreach($collection as $_product){
				$arr=explode(",",$_product->getExpiryBymonth());
				if(!in_array(19,$arr)){
				$expro .='<tr><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td><td>'.$_product->getExpiryDate().'</td></tr>';
				$_product->setStoreId(1)->setExpiryBymonth(array_unique(array_merge($arr,array(19))));
				
				$_product->save();
				}
				
				//print_r($_product->getExpiryBymonth());
			    }
			}
			//echo $expro;
			//exit;
			
			
			
			
			
			
			/* ************* expire in 4 month ************ */
			 $collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			$collection->addAttributeToFilter('expiry_date', array(
			    'from' => Date('Y-m-d', strtotime("+ 4 Months")),
			    'to' =>Date('Y-m-d', strtotime("+ 5 Months")),
			    'date' => true,
			    ));
			if($collection->count()>0){
			    
			    $expro .='<tr><td colspan="4">Following product will expire after 4 months</td>
			    
			    ';
			    foreach($collection as $_product){
				$arr=explode(",",$_product->getExpiryBymonth());
				if(!in_array(20,$arr)){
				    
				$expro .='<tr><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td><td>'.$_product->getExpiryDate().'</td></tr>';
				
				$_product->setStoreId(1)->setExpiryBymonth(array_unique(array_merge($arr,array(20))));
				$_product->save();
				}
			    }
			
			}
			
			/* ************* expire in 2 month ************ */
			 $collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			$collection->addAttributeToFilter('expiry_date', array(
			    'from' => Date('Y-m-d', strtotime("+ 2 Months")),
			    'to' =>Date('Y-m-d', strtotime("+ 3 Months")),
			    'date' => true,
			    ));
			if($collection->count()>0){
			    
			    $expro .='<tr><td colspan="4">Following product will expire after 2 months</td>
			    
			    ';
			    foreach($collection as $_product){
				$arr=explode(",",$_product->getExpiryBymonth());
				if(!in_array(21,$arr)){
				    
				$expro .='<tr><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td><td>'.$_product->getExpiryDate().'</td></tr>';
				
				$_product->setStoreId(1)->setExpiryBymonth(array_unique(array_merge($arr,array(21))));
				$_product->save();
				}
			  
			    }
			
			}
			
			
			
			/* ************* expire after 1 month ************ */
			 $collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			$collection->addAttributeToFilter('expiry_date', array(
			    'from' => Date('Y-m-d', strtotime("+ 1 Months")),
			    'to' =>Date('Y-m-d', strtotime("+ 2 Months")),
			    'date' => true,
			    ));
			//echo $collection->getSelect()->__toString();
			if($collection->count()>0){
			    
			    $expro .='<tr><td colspan="4">Following product will expire after 1 month</td>
			    
			    ';
			    foreach($collection as $_product){
				$arr=explode(",",$_product->getExpiryBymonth());
				if(!in_array(22,$arr)){
				    
				$expro .='<tr><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td><td>'.$_product->getExpiryDate().'</td></tr>';
				
				$_product->setStoreId(1)->setExpiryBymonth(array_unique(array_merge($arr,array(22))));
				$_product->save();
				}
			    }
			
			}
			
			
			
			
			
			/* ************* expire within 1 month ************ */
			 $collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			$collection->addAttributeToFilter('expiry_date', array(
			    'from' => Date('Y-m-d'),
			    'to' =>Date('Y-m-d', strtotime("+ 1 Months")),
			    'date' => true,
			    ));
			//echo $collection->getSelect()->__toString();
			if($collection->count()>0){
			    
			    $expro .='<tr><td colspan="4">Following product will expire within 1 month</td>
			    
			    ';
			    foreach($collection as $_product){
				$arr=explode(",",$_product->getExpiryBymonth());
				if(!in_array(18,$arr)){
				    
				$expro .='<tr><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td><td>'.$_product->getExpiryDate().'</td></tr>';
				
				$_product->setStoreId(1)->setExpiryBymonth(array_unique(array_merge($arr,array(18))));
				$_product->save();
				}
			    }
			
			}
			
			$expro .='</table>';
			
			echo $expro;
			
			
			
				
		$collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			$collection->addAttributeToFilter('expiry_date', array(
			    'lteq' => date("Y-m-d"),
			    
			    'date' => true,
			    ));	
			
		//echo $collection->getSelect()->__toString()	;
		
		foreach($collection as $_product){
		    $product_id=$_product->getId();
		    $storeId=1;
		    //Mage::getModel('catalog/product_status')->updateProductStatus($product_id, $storeId, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
		    
		    $categories_pd = array(5);                              
                        $_product->setStoreId(1)->setCategoryIds(array_merge($_product->getCategoryIds(),array($categories_pd)))
                        
			->setStatus(2);
			$_product->save();
			
		    
		}
		
    
   }
}