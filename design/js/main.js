$(window).load(function() {
  new WOW().init();	
});

/* Demo Scripts for Bootstrap Carousel and Animate.css article
* on SitePoint by Maria Antonietta Perna
*/
(function( $ ) {

	//Function to animate slider captions 
	function doAnimations( elems ) {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = $(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = $('#carousel-example-generic'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	}); 
	$myCarousel.carousel({interval: 2000});
	
})(jQuery);



//Owl carousel logo_slider initiation
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,	
    navText: [
      "<i class='fa fa-arrow-circle-o-left'></i>",
      "<i class='fa fa-arrow-circle-o-right'></i>"
      ],
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        1000:{
            items:4
        }
    }
});


//footer collapse
$(document).on("show.bs.collapse", ".top_foot ul.col_panel_foot", function (event) {
	var $this=$(this);
	$this.parent().find('h5 a').html('<i class="fa fa-minus"></i>');
});
$(document).on("hide.bs.collapse", ".top_foot ul.col_panel_foot", function (event) {
	var $this=$(this);
	$this.parent().find('h5 a').html('<i class="fa fa-plus"></i>');
});

//for flexslider product slider
$(window).load(function() {
// The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    //slideshow: false,
    itemWidth: 102,
    itemMargin: 30,
    asNavFor: '#slider'
  });
 
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});

//panel toggle js
$('.left_panel .panel-body').on('shown.bs.collapse',function(){
	var $this=$(this);
	$this.parent().find('.panel-heading .collapse_icn').html('<i class="fa fa-minus-square-o"></i>');
});
$('.left_panel .panel-body').on('hidden.bs.collapse',function(){
	var $this=$(this);
	$this.parent().find('.panel-heading .collapse_icn').html('<i class="fa fa-plus-square-o"></i>');
});

//width detection for left panel collapse class

var width_w=$(window).width();
if(width_w<1025){
	$('.left_panel .panel-body').removeClass('in').addClass('collapse');
	$('.left_panel .panel-heading .collapse_icn').html('<i class="fa fa-plus-square-o"></i>')	
}

//width detection for collapse nav below 768
if(width_w<768){
	$('.bot_header').addClass('collapse');
	$('.top_foot .for_collapse .col_panel_foot').addClass('collapse');

	//width detection for reload slider bxslider below 768	
	//for bx slider initiation
	var slider =$('#bx_1,#bx_2').bxSlider({
	  minSlides: 1,
	  maxSlides: 1,
	  slideWidth: 267,
	  slideMargin: 0
	});	
}
else{
	//for bx slider initiation
	var slider =$('#bx_1,#bx_2').bxSlider({
	  minSlides: 3,
	  maxSlides: 3,
	  slideWidth: 267,
	  slideMargin: 24
	});	
}