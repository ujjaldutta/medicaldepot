jQuery(document).ready(function() {
  new WOW().init();	
  //Function to animate slider captions 
	function doAnimations( elems ) {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = jQuery(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = jQuery.noConflict();
	var $myCarousel = jQuery('#carousel-example-generic'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = jQuery(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	}); 
	$myCarousel.carousel({interval: 2000});
});

/* Demo Scripts for Bootstrap Carousel and Animate.css article
* on SitePoint by Maria Antonietta Perna
*/
(function( $ ) {

	
	
})(jQuery);



//Owl carousel logo_slider initiation
jQuery(document).ready(function() {
	
jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,	
    navText: [
      "<img src='"+pic1+"'>",
      "<img src='"+pic2+"'>",
      ],
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
});

//footer collapse
jQuery(document).ready(function() {
jQuery(document).on("show.bs.collapse", ".top_foot ul.col_panel_foot", function (event) {
	var $this=jQuery(this);
	$this.parent().find('h5 a').html('<i class="fa fa-minus"></i>');
});
});
jQuery(document).ready(function() {
jQuery(document).on("hide.bs.collapse", ".top_foot ul.col_panel_foot", function (event) {
	var $this=jQuery(this);
	$this.parent().find('h5 a').html('<i class="fa fa-plus"></i>');
});
});
//for flexslider product slider
jQuery(window).load(function() {
// The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 107,
    itemMargin: 15,
   /* asNavFor: '#slider'*/
  });
 
  jQuery('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
//width detection for left panel collapse class
jQuery(document).ready(function(){
//panel toggle js
jQuery(document).on('shown.bs.collapse','.left_panel .panel-body',function(){
  //alert();
	var $this=jQuery(this);
	$this.parent().find('.panel-heading .collapse_icn').html('<i class="fa fa-minus-square-o"></i>');
	//alert($this.attr('id'));
});
jQuery(document).on('hidden.bs.collapse','.left_panel .panel-body',function(){
	var $this=jQuery(this);

	$this.parent().find('.panel-heading .collapse_icn').html('<i class="fa fa-plus-square-o"></i>');
	
});


var width_w=jQuery(window).width();
if(width_w<1025){
	jQuery('.left_panel .panel-body').removeClass('in').addClass('collapse');
	jQuery('.left_panel .panel-heading .collapse_icn').html('<i class="fa fa-plus-square-o"></i>')	
}

//width detection for collapse nav below 768
if(width_w<768){
	jQuery('.bot_header').addClass('collapse');
	jQuery('.top_foot .for_collapse .col_panel_foot').addClass('collapse');

	//width detection for reload slider bxslider below 768	
	//for bx slider initiation
	var slider =jQuery('#bx_1,#bx_2').bxSlider({
	  minSlides: 1,
	  maxSlides: 1,
	  slideWidth: 267,
	  slideMargin: 0
	});	
	
	
jQuery('.bot_header .nav.navbar-nav li a .caret').on('click',function(e){
  e.preventDefault();
  var $this=$(this);
  $this.parent().parent().children('ul').slideToggle();
  $this.toggleClass('menu_op');
 });	
	
	
}

else{
	//for bx slider initiation
	var slider =jQuery('#bx_1,#bx_2').bxSlider({
	  minSlides: 3,
	  maxSlides: 3,
	  slideWidth: 267,
	  slideMargin: 24
	});	
}


  jQuery(document).on('click','.addtocart2',function(event){
			//event.preventDefault();
			if(jQuery(this).attr('disabled')) {
			  return false;
			  
			}
			var url = jQuery(this).attr('url');
                        var pid = jQuery(this).attr('pid');
                        var addcartblock= jQuery(this);
			jQuery(".loading").show();
                        jQuery(this).attr('disabled', true);
                       	
			
			jQuery.ajax({
					url: url,
					dataType: 'json',
					type : 'post',
                    data :'qty=1&product='+pid,
				success: function(data){

                                       addcartblock.attr('disabled', false);     
		                            var cart = jQuery('.skip-link2, .skip-link');
			             var imgtodrag = jQuery(addcartblock).parent().parent().find("img").eq(0);
		                                             
			             if (imgtodrag && data.status=='added') {
			                 var imgclone = imgtodrag.clone()
			                     .offset({
			                     top: imgtodrag.offset().top,
			                     left: imgtodrag.offset().left
			                 })
			                     .css({
			                     'opacity': '0.5',
			                         'position': 'absolute',
			                         'height': '477px',
			                         'width': '357px',
			                         'z-index': '1033'
			                 })
			                     .appendTo(jQuery('body'))
			                     .animate({
			                     'top': cart.offset().top + 10,
			                         'left': cart.offset().left + 10,
			                         'width': 75,
			                         'height': 75
			                 }, 1000, function(){
                                               if (jQuery('.count').length) {
						jQuery('.count').remove();
						}
                                               jQuery(".trigger_mini").append('<span class="count">'+data.count+'</span>');
                                               jQuery(".top-link-cart").html('My Cart'+'(' + data.count +')');

                                               if(jQuery( window ).width()<768){
                                               		imgclone.animate({
                                                      'width': 0,
                                                      'height': 0
                                                  }, function () {
                                                      jQuery(this).detach();
                                                  });
                                               	return;
                                               }
                                                jQuery(cart).addClass("skip-active");
                                                jQuery(cart).next().hide();
                                                jQuery("#header-cart").addClass("skip-active");
                                                jQuery("#header-cart").slideDown("slow");
                                                
                                                jQuery("#header-cart").html(data.content);

                                                  imgclone.animate({
                                                      'width': 0,
                                                      'height': 0
                                                  }, function () {
                                                      jQuery(this).detach();
                                                  });
						   jQuery('html, body').animate({ scrollTop: 0 }, 'slow', function () {
						    
						      
						  });
                                             
                                         });
         
								                 
									}
				
							   jQuery(".loading").hide();
                                            }
		
                                    });
			//return false;
                           
       });
 
  
  jQuery('.addtocart').click(function(event){
			event.preventDefault();
			
			if(jQuery(this).attr('disabled')) {
			  return false;
			  
			}
			var url = jQuery(this).attr('href');
                        var pid = jQuery(this).attr('pid');
                        var addcartblock= jQuery(this);
			jQuery(".loading").show();
			
			
                        jQuery(this).attr('disabled', true);
                      
		
			jQuery.ajax({
					url: url,
					dataType: 'json',
					type : 'post',
                    data :'qty=1&product='+pid,
				success: function(data){
				   addcartblock.attr('disabled', false);
                                             
		                            var cart = jQuery('.skip-link2, .skip-link');
		                            

			             var imgtodrag = jQuery(addcartblock).parent().parent().find("img").eq(0);
		                                             
			             if (imgtodrag && data.status=='added') {
			                 var imgclone = imgtodrag.clone()
			                     .offset({
			                     top: imgtodrag.offset().top,
			                     left: imgtodrag.offset().left
			                 }).css({
			                     'opacity': '0.5',
			                         'position': 'absolute',
			                         'height': '477px',
			                         'width': '357px',
			                         'z-index': '1033'
			                 }).appendTo(jQuery('body'))
			                     .animate({
			                     'top': cart.offset().top + 10,
			                         'left': cart.offset().left + 10,
			                         'width': 75,
			                         'height': 75
			                 }, 1000, function(){
                                               if (jQuery('.count').length) {
						jQuery('.count').remove();
						}
                                               jQuery(".trigger_mini").append('<span class="count">'+data.count+'</span>');
                                               jQuery(".top-link-cart").html('My Cart'+'(' + data.count +')');

                                               if(jQuery( window ).width()<768){
                                               		imgclone.animate({
                                                      'width': 0,
                                                      'height': 0
                                                  }, function () {
                                                      jQuery(this).detach();
                                                  });
                                               	return;
                                               }
                                                jQuery(cart).addClass("skip-active");
                                                jQuery(cart).next().hide();
                                                jQuery("#header-cart").addClass("skip-active");
                                                jQuery("#header-cart").slideDown("slow");
                                                
                                                jQuery("#header-cart").html(data.content);

                                                  imgclone.animate({
                                                      'width': 0,
                                                      'height': 0
                                                  }, function () {
                                                      jQuery(this).detach();
                                                  });
						   jQuery('html, body').animate({ scrollTop: 0 }, 'slow', function () {
						      
						  });
                                             
                                         });
         
								                 
									}
				
							   jQuery(".loading").hide();
                                            }
		
                                    });
		
                           
       });



jQuery('.btn-cart').click(function(event){
			event.preventDefault();
			if(jQuery(this).attr('disabled')) {
			  return false;
			  
			}
			
			var url = jQuery("#product_addtocart_form").attr('action');
                        var pid = jQuery(this).attr('pid');
                        var addcartblock= jQuery(this);
			var opval=jQuery(".super-attribute-select").val();
			if (configpro==1 && opval=='') {
				jQuery(".super-attribute-select").css({border:'1px solid #FF0000'})
				return false;
			}
			jQuery(".super-attribute-select").css({border:'1px solid #FFFFFF'})
			jQuery("#loading").show();
                         jQuery(this).attr('disabled', true);
                       
					/*jQuery('html, body').animate({scrollTop:0}, 'slow');*/
				
					jQuery.ajax({
							url: url,
							dataType: 'json',
							type : 'post',
                                                        data :jQuery("#product_addtocart_form").serialize(),
						    success: function(data){
							 addcartblock.attr('disabled', true);
                                                                         
                                                        var cart = jQuery('.skip-link2, .skip-link');
								             var imgtodrag = jQuery("#image1");
                                                                            // alert(jQuery(imgtodrag).attr("src"));
                                                                            // alert(jQuery(addcartblock).attr("class"));
                                                                            // alert(jQuery(imgtodrag).attr("class"));
								             if (imgtodrag && data.status=='added') {
								                 var imgclone = imgtodrag.clone()
								                     .offset({
								                     top: imgtodrag.offset().top,
								                     left: imgtodrag.offset().left
								                 })
								                     .css({
								                     'opacity': '0.5',
								                         'position': 'absolute',
								                         'height': '477px',
								                         'width': '357px',
								                         'z-index': '1033'
								                 })
								                     .appendTo(jQuery('body'))
								                     .animate({
								                     'top': cart.offset().top + 10,
								                         'left': cart.offset().left + 10,
								                         'width': 75,
								                         'height': 75
								                 }, 1000, function(){
                                                                                     
                                                                                       jQuery(".count").html(data.count);
                                                                                        jQuery(".top-link-cart").html('My Cart'+'(' + data.count +')');
                                                                                        jQuery(cart).addClass("skip-active");
                                                                                        jQuery(cart).next().hide();
                                                                                        jQuery("#header-cart").addClass("skip-active");
                                                                                        jQuery("#header-cart").slideDown("slow");
                                                                                        
                                                                                        jQuery("#header-cart").html(data.content);

                                                                                          imgclone.animate({
                                                                                              'width': 0,
                                                                                                  'height': 0
                                                                                          }, function () {
                                                                                              jQuery(this).detach();
                                                                                          });
                                                                                     
                                                                                 });
                                                                                 
							
									
									}
							
					
						
                                                        //alert("Product is added to cart successfully.");
                                                         jQuery("#loading").hide();
                                                        }
					
                                                });
                                       
       });

       jQuery('.sort-drop').focus(function(){
	    jQuery(".select_drop").addClass("selected");
	}).blur(function(){
	    jQuery(".select_drop").removeClass("selected");
	});
   
       

 });
jQuery(window).load(function(){
	
	
	
jQuery('.image_zoom').elevateZoom(); 
	//jQuery('.image_zoom').elevateZoom();
	});